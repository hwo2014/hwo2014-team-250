package main

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestCreateTrace(t *testing.T) {

	var msg Msg
	msg.Data = json.RawMessage(trackdata)
	game, err := CreateMap(&msg)
	if err != nil {
		t.Fatalf("Error %s", err)
		fmt.Println(game)
	}

	//	fmt.Println("Game %s", GameString(game))
}

func TestPieceLen(t *testing.T) {
	/*
		var pieces []Piece
		err := json.Unmarshal([]byte(piecesjson), &pieces)
		if err != nil {
			t.Fatalf("Err %s", err)
		}

		for i, p := range pieces {
			fmt.Printf("piece %d len %.2f angle %.2f radius %d\n", i, p.Len(-10), p.Angle, p.Radius)
			fmt.Printf("piece %d len %.2f angle %.2f radius %d\n", i, p.Len(10), p.Angle, p.Radius)
		}
	*/
}

var (
	trackdata = `{"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"Bulkegjengen","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"00ad6799-4a9b-4493-86fd-a228261b15fc"}`

	piecesjson = `[
               {
                  "length":100.0
               },
               {
                  "length":100.0
               },
               {
                  "length":100.0
               },
               {
                  "length":100.0,
                  "switch":true
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":200,
                  "angle":22.5,
                  "switch":true
               },
               {
                  "length":100.0
               },
               {
                  "length":100.0
               },
               {
                  "radius":200,
                  "angle":-22.5
               },
               {
                  "length":100.0
               },
               {
                  "length":100.0,
                  "switch":true
               },
               {
                  "radius":100,
                  "angle":-45.0
               },
               {
                  "radius":100,
                  "angle":-45.0
               },
               {
                  "radius":100,
                  "angle":-45.0
               },
               {
                  "radius":100,
                  "angle":-45.0
               },
               {
                  "length":100.0,
                  "switch":true
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":200,
                  "angle":22.5
               },
               {
                  "radius":200,
                  "angle":-22.5
               },
               {
                  "length":100.0,
                  "switch":true
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "length":62.0
               },
               {
                  "radius":100,
                  "angle":-45.0,
                  "switch":true
               },
               {
                  "radius":100,
                  "angle":-45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "radius":100,
                  "angle":45.0
               },
               {
                  "length":100.0,
                  "switch":true
               },
               {
                  "length":100.0
               },
               {
                  "length":100.0
               },
               {
                  "length":100.0
               },
               {
                  "length":90.0
               }
            ]`
	piece2 = ``
)
