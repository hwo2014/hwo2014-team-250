package main

import (
	"encoding/json"
	"fmt"
	_ "log"
)

type Tick struct {
	Cars []*CarPos `json:"data"`
}

type CarPos struct {
	Id       Car      `json:"id"`
	Angle    float32  `json:"angle"`
	PiecePos PiecePos `json:"piecePosition"`
	Lap      int      `json:"lap"`
}

type Car struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

type PiecePos struct {
	Index       int     `json:"pieceIndex"`
	InPieceDist float64 `json:"inPieceDistance"`
	Lane        CarLane `json:"lane"`
}

type CarLane struct {
	Start int `json:"startLaneIndex"`
	End   int `json:"endLaneIndex"`
}

func CreateTick(msg *Msg) (*Tick, error) {
	tick := &Tick{}
	err := json.Unmarshal(msg.Data, tick)
	if err != nil {
		return nil, err
	}
	return tick, nil
}

func CreatePosition(cp *CarPos) string {
	return fmt.Sprintf("Car %s Piece %d Pos %.3f", cp.Id.Name, cp.PiecePos.Index, cp.PiecePos.InPieceDist)
	d, err := json.MarshalIndent(cp, "", "  ")
	if err != nil {
		return "error"
	}
	return string(d)
}

func CreateString(car *CarPos, m *Map) string {
	return fmt.Sprintf("Car %s Piece %d Pos %.3f", car.Id.Name, car.PiecePos.Index, car.PiecePos.InPieceDist)
}
