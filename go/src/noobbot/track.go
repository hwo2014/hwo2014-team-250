package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
)

type Map struct {
	Track   Track   `json:"track"`
	Cars    []Car   `json:"cars"`
	Session Session `json:"raceSession"`
}

func (g *Map) String() string {
	d, err := json.MarshalIndent(g, "", "  ")
	if err != nil {
		return "error"
	}
	return string(d)
}

type Session struct {
	Laps         int  `json:"laps"`
	MaxLapTimeMs int  `json:"maxLapTimeMs"`
	QuickRace    bool `json:"quickRace"`
}

type Track struct {
	Id     string  `json:"id"`
	Name   string  `json:"name"`
	Pieces []Piece `json:"pieces"`
	Lanes  []Lane  `json:"lanes"`
}

type Lane struct {
	Offset int `json:"distanceFromCenter"`
	Index  int `json:"index"`
}

func (t *Track) String() string {
	return fmt.Sprintf("Track %s - %d pieces - %d long %d lanes", t.Name, len(t.Pieces), t.Len(), len(t.Lanes))
}

func (t *Track) Len() float64 {
	sum := float64(0)
	for i := 0; i < len(t.Pieces); i++ {
		sum += t.Pieces[i].Len(0)
	}
	return sum
}

type PieceType uint8

const (
	Straight PieceType = iota
	Bend
)

func (p *Piece) fmt() {
	fmt.Println("")
}

type Piece struct {
	Length float64 `json:"length"`
	Switch bool    `json:"switch"`
	Angle  float64 `json:"angle"`
	Radius int     `json:"radius"`
}

func (p *Piece) Type() PieceType {
	if p.Length > 0 {
		return Straight
	}
	return Bend
}

func (p *Piece) Len(offset int) float64 {
	if p.Length > 0 {
		return float64(p.Length)
	}
	rad := p.Radius + offset
	percentof := math.Abs(p.Angle) / 360
	circumference := 2 * math.Pi * float64(rad)
	return circumference * percentof
}

func CreateMap(msg *Msg) (*Map, error) {
	var m map[string]interface{}
	if err := json.Unmarshal(msg.Data, &m); err != nil {
		return nil, err
	}
	log.Println("m", m)

	var game Map
	d, err := json.Marshal(m["data"].(map[string]interface{})["race"])
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(d, &game)
	if err != nil {
		return nil, err
	}
	return &game, nil
}
