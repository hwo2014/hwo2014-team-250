package main

func (s *State) GetPosition(car *CarPos, i int) *Tick {
	if len(s.ticks) == 1 {
		return s.ticks[0]
	}
	return s.ticks[len(s.ticks)+i-1]
}
func distance(car2, car1 *CarPos, track Track) float64 {
	p1, p2 := car1.PiecePos, car2.PiecePos
	if p1.Index != p2.Index {
		return track.Pieces[p1.Index].Len(0) - float64(p1.InPieceDist) + float64(p2.InPieceDist)
	}
	return float64(p2.InPieceDist - p1.InPieceDist)
}

func DistanceToTurn(car *CarPos, game *State) float64 {
	dist := float64(0)
	i := car.PiecePos.Index
	for {
		p := game.gamemap.Track.Pieces[i%len(game.gamemap.Track.Pieces)]
		if p.Type() == Bend {
			return dist
		}
		dist += p.Len(0)
		i++
	}
	return 0
}

func Speed(car *CarPos, game *State) float64 {
	if len(game.ticks) > 0 {
		tick, lasttick := game.GetPosition(car, 0), game.GetPosition(car, -1)
		car, lastcar := tick.Cars[0], lasttick.Cars[0]
		return distance(car, lastcar, game.gamemap.Track)
	}
	return 0
}

func distance_left(car *CarPos, piece *Piece) float64 {
	return piece.Length - float64(car.PiecePos.InPieceDist)
}
