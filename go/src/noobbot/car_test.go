package main

import (
	"encoding/json"
	_ "fmt"
	"testing"
)

func TestMAp(t *testing.T) {

	var msg Msg
	msg.Data = json.RawMessage(carData)
	_, err := CreateTick(&msg)
	if err != nil {
		t.Fatalf("err %s", err, string(carData))
	}
}

var (
	carData []byte = []byte(`
      {
   "msgType":"carPositions",
   "data":[
      {
         "id":{
            "name":"Bulkegjengen",
            "color":"red"
         },
         "angle":0.0,
         "piecePosition":{
            "pieceIndex":0,
            "inPieceDistance":0.0,
            "lane":{
               "startLaneIndex":0,
               "endLaneIndex":0
            },
            "lap":0
         }
      }
   ],
   "gameId":"0519f797-ac8d-40f8-84b2-13181a008f0b"
}`)
)
