package main

import (
	_ "encoding/json"
)

type Msg struct {
	MsgType string `json:"msgType"`
	Data    []byte `json:"-"`
}
