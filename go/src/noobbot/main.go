package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
)

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

var (
	dump     = true
	dumpchan = make(chan string)
)

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float32) (err error) {
	err = write_msg(writer, "throttle", throttle)
	return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	data := make(map[string]string)
	data["name"] = name
	data["key"] = key
	err = write_msg(writer, "join", data)
	return
}

func read_msg(reader *bufio.Reader) (*Msg, error) {
	data, err := reader.ReadBytes('\n')
	if err != nil {
		return nil, err
	}
	if dump {
		go func() {
			dumpchan <- string(data)
		}()
	}
	msg := &Msg{Data: data}
	err = json.Unmarshal(data, &msg)
	if err != nil {
		return nil, err
	}
	return msg, nil
}

func bot_loop(conn net.Conn, name string, key string) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	send_join(writer, name, key)
	state := newState(writer)
	for {
		msg, err := read_msg(reader)
		if err != nil {
			log.Println("fail tail")
			log_and_exit(err)
		}
		err = state.Process(msg)
		if err != nil {
			log_and_exit(err)
		}
	}
}

func parse_args() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {

	if dump {
		go func() {
			f, err := os.Create("C:/Log/dump")
			if err != nil {
				log.Fatalf("Error opening dump file %s", err)
			}
			for {
				line := <-dumpchan
				f.Write([]byte(line))
			}
		}()
	}

	host, port, name, key, err := parse_args()

	if err != nil {
		log_and_exit(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	err = bot_loop(conn, name, key)
}
