package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
)

type State struct {
	w       *bufio.Writer
	gamemap *Map
	tick    int
	ticks   []*Tick
	trottle []float64
}

func (s *State) CurrentState() string {
	return s.State(len(s.ticks) - 1)
}

func (s *State) CurrentTick() *Tick {
	return s.ticks[len(s.ticks)-1]
}

func (s *State) State(i int) string {
	tick := s.ticks[i]
	car := tick.Cars[0]
	return fmt.Sprintf("Tick %d Piece %d PiecePosition %.2f PLen %.2f Speed %.2f Agnle %.2f", s.tick, car.PiecePos.Index, car.PiecePos.InPieceDist, s.gamemap.Track.Pieces[car.PiecePos.Index].Length, Speed(car, s), car.Angle)
}

func newState(w *bufio.Writer) *State {
	s := &State{w: w}
	return s
}

func (s *State) GetPiece(i int) *Piece {
	i = (i + 1) % len(s.gamemap.Track.Pieces)
	return &s.gamemap.Track.Pieces[i]
}

func (s *State) GetThrottle(currrent float64) float64 {
	car := s.CurrentTick().Cars[0]
	speed := Speed(car, s)

	piece, next := s.GetPiece(car.PiecePos.Index), s.GetPiece(car.PiecePos.Index+1)
	distance_to_next := piece.Len(0) - car.PiecePos.InPieceDist
	isnext := speed >= distance_to_next

	if next.Type() == Bend && DistanceToTurn(car, s) < speed*12 && speed > 6 {
		return 0.10
	}
	if speed <= 6.5 {
		return 1.0
	}
	if isnext && next.Type() == Straight {
		return 1.0
	} else if piece.Type() == Straight && next.Type() == Straight {
		return 1.0
	} else if piece.Type() == Straight && distance_to_next > 2*speed {
		return 1.0
	}
	return math.Max(0.6, currrent*0.7)
}

func (s *State) Process(msg *Msg) error {
	switch msg.MsgType {
	case "join":
		log.Printf("Joined")
		send_ping(s.w)
	case "gameStart":
		send_ping(s.w)
	case "crash":
		log.Printf("Someone crashed %v", msg.Data)
		os.Exit(0)
	case "gameEnd":
		log.Printf("Game ended")
		os.Exit(0)
	case "carPositions":
		if tick, err := CreateTick(msg); err != nil {
			log.Fatalf("Erro parsing position %s", err)
		} else {
			s.ticks = append(s.ticks, tick)
		}
		if i := len(s.trottle); i > 0 {
			throttle := s.GetThrottle(s.trottle[len(s.trottle)-1])
			s.trottle = append(s.trottle, throttle)
			log.Printf("State %s --- Throttle set %.2f", s.CurrentState(), throttle)
			send_throttle(s.w, float32(throttle))
		} else {
			send_throttle(s.w, 1.0)
			s.trottle = append(s.trottle, 1.0)
		}
		s.tick++
	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", msg.Data))
		send_ping(s.w)
	case "gameInit":
		if gamemap, err := CreateMap(msg); err != nil {
			log.Fatalf("Error parsing track %s", err)
		} else {
			s.gamemap = gamemap
			log.Printf("Starting game %s", s.gamemap.String())
		}
		send_ping(s.w)
	default:
		log.Printf("Got msg type: %s", msg.MsgType)
		send_ping(s.w)
	}
	return nil
}
